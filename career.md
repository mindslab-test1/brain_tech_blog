---
layout: page
title: Career 버튼은 어디에?
permalink: /career/
---


자네, 어쩌다 이런 페이지까지 흘러들어왔는가?

어지간히 눈썰미가 좋지 않은 모양이구만.

지원 버튼은 이 tech blog 의 우측 상단, Publication 우측의 Career 를 누르면 MINDs Lab Brain 팀에 지원할 수 있다네.

### 그럼 이만, 찡긋!

![와우할아버지]({{"/assets/yjyimg/wowgrandpa.jpg"| relative_url}}){: width="100" height="100"}
