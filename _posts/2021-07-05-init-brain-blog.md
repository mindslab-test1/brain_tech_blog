---
layout: post
title: "__init__(brain, blog):"
description: "Hello community! 마인즈랩 브레인이 블로그를 시작합니다"
categories: news
author: MINDs Lab
github: mindslab-ai
---

![Brain Algorithm Session](/assets/2021-07-05-init-brain-blog/algo-session.jpg)
## 마인즈랩 브레인이 블로그를 시작합니다

### Hello Community!
안녕하세요 **MINDs Lab Brain**입니다. 
Brain팀은 [**MINDs Lab**](https://maum.ai)의 연구조직으로, 회사에서 서비스하(려)는 ML/DL 알고리즘을 연구 및 개발하고 있습니다.    

Brain팀에서는 수학, 컴퓨터공학, 물리, 뇌공학 등 다양한 배경을 가진 연구원들이 모여 Audio, NLP, Vision 분야를 누비며 연구를 진행 중입니다. 연구원들 분들 중에는 DL/ML 분야 연구실에서 석사 과정을 졸업하신 분부터 국제 수학 올림피아드 국가대표를 하실 정도로 수학을 잘하시는 분, 전공과는 무관하게 DL/ML에 대한 열정으로 스터디를 하시는 분 등 다양하면서도 포텐셜 넘치시는 분들이 함께 시너지를 만들고 있습니다 :)

그리고 이 시너지를 바탕으로 서비스에 대한 연구를 진행함은 물론, Interspeech, ECCV, NeurIPS, CVPR 등의 학회 및 Workshop에서 논문 발표 및 Challenge 수상을 하고 있습니다!

### Brain Blog?
Brain팀의 연구는 AI research community의 많은 글과 오픈소스로부터 도움을 받아 이뤄지고, 또한 [논문과 코드 공개를 통해 기여](/publication)하고 있습니다. 사실 논문과 코드라는 결실은 Brain팀에서의 수많은 토의와 아이디어들을 기반으로 맺게 되는데요. Brain팀에서 진행하는 DL/ML에 대한 이야기들 중 가치 있는 이야기들을 모아 이 블로그에서 공유하려고 합니다. 물론, 논문과 코드 공개 소식도 이 블로그에서 제일 먼저 공유할 예정입니다! 

### In This Blog
위에서도 잠깐 말씀드렸지만, MINDsLab Brain팀 블로그에서는 아래와 같은 내용들을 공유하려고 합니다. 
- AI 연구 이야기, 그리고 그 과정에서 우리가 발견한 것들
- 논문 및 학회 참석에 대한 리뷰
- Research in the wild
- 그리고 DL/ML에 대한 잡담...!

### Brain팀은 오늘도 순항 중!
오늘도 Brain팀 연구원분들은 여러 연구와 Code-work을 진행하고 있습니다.  
Brain팀처럼 딥러닝 모델을 만들고 계신 분들부터 DL/ML에 대한 열정으로 달리기를 시작하시려는 분들까지!   
Brain팀 블로그로 놀러 오셔서 함께 이야기해요! 반갑습니다 :+1: