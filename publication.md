---
layout: page
title: Publications
permalink: /publications/
---

<!-- ![Minds Lab Logo](assets/mindslab_logo.png) -->
## List of Publications & Open Source Activities
#### **(June 2021)** _Sharp Edge Recovery via SE(3)-Equivariant Network_
- Presented at 2nd SHApe Recovery from Partial textured 3D scans at *CVPR 2021*
- [talk](https://youtu.be/UVYQzQ-mH1w?t=9231)
- 2nd place at SHAPR 2021 Challenge track 3

#### **(April 2021)** _NU-Wave: A Diffusion Probabilistic Model for Neural Audio Upsampling_
- Accepted to *Interspeech 2021*
- [arXiv:2104.02321](https://arxiv.org/abs/2104.02321)
- [audio samples](https://mindslab-ai.github.io/nuwave)
- [github](https://github.com/mindslab-ai/nuwave)
    <iframe src="https://ghbtns.com/github-btn.html?user=mindslab-ai&repo=nuwave&type=star&count=true" frameborder="0" scrolling="0" width="150" height="20" title="GitHub"></iframe>

#### **(April 2021)** _Assem-VC: Realistic Voice Conversion by Assembling Modern Speech Synthesis Techniques_
- [arXiv:2104.00931](https://arxiv.org/abs/2104.00931)
- [audio samples](https://mindslab-ai.github.io/assem-vc)
- [github](https://github.com/mindslab-ai/assem-vc)
    <iframe src="https://ghbtns.com/github-btn.html?user=mindslab-ai&repo=assem-vc&type=star&count=true" frameborder="0" scrolling="0" width="150" height="20" title="GitHub"></iframe>

#### **(December 2020)** _DS4C Patient Policy Province Dataset: A Comprehensive COVID-19 Dataset for Causal and Epidemiological Analysis_
- Accepted to Causal Discovery and Causality-Inspired Machine Learning *Workshop at NeurIPS 2020* 
- [paper link](https://www.cmu.edu/dietrich/causality/CameraReadys-accepted%20papers/55%5cCameraReady%5cpaper.pdf)

#### **(September 2020)** _3D Room Layout Estimation Beyond the Manhattan World Assumption_
- Presented at Holistic Scene Structures for 3D Vision at *ECCV 2020* 
- [arXiv:2009.02857](https://arxiv.org/abs/2009.02857)
- 3rd place at Holistic 3D Vision Challenge track 1

#### **(May 2020)** _Cotatron: Transcription-Guided Speech Encoder for Any-to-Many Voice Conversion without Parallel Data_
- Accepted to *Interspeech 2020* 
- [arXiv:2005.03295](https://arxiv.org/abs/2005.03295)
- [audio samples](https://mindslab-ai.github.io/cotatron/)
- [github](https://github.com/mindslab-ai/cotatron)
    <iframe src="https://ghbtns.com/github-btn.html?user=mindslab-ai&repo=cotatron&type=star&count=true" frameborder="0" scrolling="0" width="150" height="20" title="GitHub"></iframe>
- [talk](https://youtu.be/lnNuL8hqoh4)

## Open-Source Activities
#### **(July 2021)** WaveGrad2
- First successful open-source implementation of *WaveGrad2* [arXiv:2106.09660](https://arxiv.org/abs/2106.09660)
    <iframe src="https://ghbtns.com/github-btn.html?user=mindslab-ai&repo=wavegrad2&type=star&count=true" frameborder="0" scrolling="0" width="150" height="20" title="GitHub"></iframe>
    
#### **(October 2020)** FaceShifter
- First successful open-source implementation of *FaceShifter* [arXiv:1912.13457](https://arxiv.org/abs/1912.13457). 
    <iframe src="https://ghbtns.com/github-btn.html?user=mindslab-ai&repo=faceshifter&type=star&count=true" frameborder="0" scrolling="0" width="150" height="20" title="GitHub"></iframe>

#### **(June 2020)** KoTDG
- Korean Text Data Generator for OCR tasks.
    <iframe src="https://ghbtns.com/github-btn.html?user=Diuven&repo=KoTDG&type=star&count=true" frameborder="0" scrolling="0" width="150" height="20" title="GitHub"></iframe>

#### **(March 2020)** Data-Science-for-COVID-19
- COVID-19 Korea Dataset with patient routes and visualizer
- Co-led the collaboration project.
- News coverage: [Link 1](http://it.chosun.com/site/data/html_dir/2020/06/24/2020062403706.html) [Link 2](http://www.aitimes.kr/news/articleView.html?idxno=15685)
    <iframe src="https://ghbtns.com/github-btn.html?user=ThisIsIsaac&repo=Data-Science-for-COVID-19&type=star&count=true" frameborder="0" scrolling="0" width="150" height="20" title="GitHub"></iframe>

#### **(January 2020)** Reformer-pytorch
- Implementation of *Reformer: The Efficient Transformer* [arXiv:2001.04451](https://arxiv.org/abs/2001.04451) in pytorch.
    <iframe src="https://ghbtns.com/github-btn.html?user=Rick-McCoy&repo=Reformer-pytorch&type=star&count=true" frameborder="0" scrolling="0" width="150" height="20" title="GitHub"></iframe>

#### **(October 2019)** MelGAN
- Implementation of *MelGAN* vocoder [arXiv:1910.06711](https://arxiv.org/abs/1910.06711) (compatible with [NVIDIA/tacotron2](https://github.com/NVIDIA/tacotron2))
    <iframe src="https://ghbtns.com/github-btn.html?user=seungwonpark&repo=melgan&type=star&count=true" frameborder="0" scrolling="0" width="150" height="20" title="GitHub"></iframe>

#### **(August 2019)** MelNet
- Implementation of *MelNet: A Generative Model for Audio in the Frequency Domain* [arXiv:1906.01083](https://arxiv.org/abs/1906.01083).
- Work done with Deepest AI (SNU Deep Learning Society).
    <iframe src="https://ghbtns.com/github-btn.html?user=Deepest-Project&repo=MelNet&type=star&count=true" frameborder="0" scrolling="0" width="150" height="20" title="GitHub"></iframe>

#### **(April 2019)** RandWireNN
- Implementation of *Exploring Randomly Wired Neural Networks for Image Recognition* [arXiv:1904.01569](https://arxiv.org/abs/1904.01569).
    <iframe src="https://ghbtns.com/github-btn.html?user=seungwonpark&repo=RandWireNN&type=star&count=true" frameborder="0" scrolling="0" width="150" height="20" title="GitHub"></iframe>

#### **(March 2019)** VoiceFilter
- First successful open-source implementation of Google's *VoiceFilter* [arXiv:1810.04826](https://arxiv.org/abs/1810.04826).
    <iframe src="https://ghbtns.com/github-btn.html?user=mindslab-ai&repo=voicefilter&type=star&count=true" frameborder="0" scrolling="0" width="150" height="20" title="GitHub"></iframe>

#### **(August 2018 – August 2020)** Contribution to TensorFlow, PyTorch, Flashlight
- Made numerous contribution to the most popular deep learning frameworks, primarily by [Isaac Lee](https://github.com/ThisIsIsaac) (former CUDA Team leader). His name was proudly mentioned in the list of contributors at *TensorFlow* release note: [Link](https://github.com/tensorflow/tensorflow/releases/tag/v2.1.0)
